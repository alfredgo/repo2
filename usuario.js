function guardarDatosUsuario() {
  var nombre = document.getElementById("txtNombre").value;
  var email = document.getElementById("txtEmail").value;
  var dni = document.getElementById("txtDni").value;
  localStorage.setItem("nombre", nombre);
  sessionStorage.setItem("nombre", nombre);
  localStorage.setItem("email", email);
  sessionStorage.setItem("email", email);
  localStorage.setItem("dni", dni);
  sessionStorage.setItem("dni", dni);
  var usuario = {"nombre":nombre,"email":email,"dni":dni};
  localStorage.setItem("usuarioST", JSON.stringify(usuario));
  sessionStorage.setItem("usuarioST", JSON.stringify(usuario));
}

function recuperarDatosUsuario() {
  var local = localStorage.getItem("nombre");
  var session = sessionStorage.getItem("nombre");
  var localEmail = localStorage.getItem("email");
  var sessionEmail = sessionStorage.getItem("email");
  var localDni = localStorage.getItem("dni");
  var sessionDni = sessionStorage.getItem("dni");
  var localJson = localStorage.getItem("usuarioST");
  var sessionJson = sessionStorage.getItem("usuarioST");
  alert("Local: " + local + ", " + localEmail + ", " + localDni + ". Session: " + session + ", " + sessionEmail + ", " + sessionDni);
  alert("Local: " + localJson + " Session: " + sessionJson);
}
